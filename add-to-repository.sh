#!/bin/bash

set -ueo pipefail

DEPLOY_DIR=/deploydir
#CI_COMMIT_SHA=7d8727654872baae9098efeecea7205c03f4edf1

declare -a tags=($(git tag -l --points-at $CI_COMMIT_SHA))

declare -a qualified_tags=()

for tag in "${tags[@]}"
do
  echo -n $tag
  if [[ "$tag" =~ ^v[0-9]+\.[0-9]+\.[0-9]+$ ]]; then 
    echo " qualified !" 
    qualified_tags+=($tag)
  else 
    echo " not qualified"
  fi
done

if [[ ${#qualified_tags[@]} == 0 ]] ; then
  echo no suitable tag found !
  exit 1
fi

if [[ ${#qualified_tags[@]} != 1 ]] ; then
  echo too much suitable tags found !
  exit 1
fi

tag=${qualified_tags[0]}

echo "We will deploy $tag"

if [ ! -d "${DEPLOY_DIR}" ] ; then
  echo "${DEPLOY_DIR} does not exist"
  exit 1
fi 

if [ ! -d "${DEPLOY_DIR}/linux-64" ] ; then
  mkdir "${DEPLOY_DIR}/linux-64"
fi

cp *.tar.bz2 $DEPLOY_DIR/linux-64/

conda index $DEPLOY_DIR
